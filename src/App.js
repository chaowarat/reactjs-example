import React, { Component } from 'react';
import './App.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';

injectTapEventPlugin();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  _handleToggle = () => this.setState({open: !this.state.open})

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <AppBar title="My example web application );"
            onLeftIconButtonTouchTap={this._handleToggle}
           />
        </MuiThemeProvider>
        <MuiThemeProvider>
          <Drawer open={this.state.open}>
            <MenuItem onTouchTap={this._handleToggle}>Menu Item</MenuItem>
            <MenuItem onTouchTap={this._handleToggle}>Menu Item 2</MenuItem>
          </Drawer>
        </MuiThemeProvider>
      </div>      
    );
  }
}

export default App;
